import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.BufferedReader;

public class CashRegister
{
   public static void main(String[] args)
   {
	    //declare variables
		String s, c, cashRegFloat, itemName, itemCost, tendered, answer, allItems, askPaymentType, keepShopping;
    	double balance, change, profit = 0;
		Transaction trans = null;
		FileWriter fileWriter = null;
		PrintWriter printWriter = null;
		BufferedReader bufferReader = null;
	    Scanner in = new Scanner(System.in);
		
		//create a file
		File saveFile = new File("saveFile.txt");
		boolean exists = saveFile.exists();
		if(!exists){
			try{
				saveFile.createNewFile();
				//System.out.println("new file");
			}catch(Exception e){
				System.out.println("error message: " + e.toString());
			}
		}
		
		try{
			
			fileWriter = new FileWriter(saveFile, true);
			printWriter = new PrintWriter(fileWriter);
		}catch(Exception e){
			System.out.println("error message: " + e.toString());
		}
		
	  
		//shoppingCart for mulitple items
		ArrayList<Transaction> shoppingCart = null;

		//Welcome message
		System.out.println("Hello, welcome to the cash register\n");

	    
		//prompt user for register amount
	    System.out.print("Please enter cash register's float: $");
		cashRegFloat = in.nextLine();
		balance = Double.parseDouble(cashRegFloat);
		  
		//menu
		while(true){
			do {	  
				System.out.println("Would you like to \'[e]xit\' or \'[p]rocess\'");
				answer = in.nextLine();
			}while( !answer.equals("exit") && !answer.equals("process") && !answer.equals("e") && !answer.equals("p"));

			if(answer.equals("exit") || answer.equals("e")) 
				break;

			//add item to cart
			shoppingCart = new ArrayList<Transaction>();
			
			do {
				
				System.out.print("Please enter the item's name: ");
				itemName = in.nextLine();

				do {
					System.out.print("Please enter the item's cost: $");
					itemCost = in.nextLine();
				//use regular expression to check if user enters correct values
				}while (!itemCost.matches("[0-9]+(\\.[0-9]([0-9])?)?"));
				
				trans = new Transaction(itemName, Double.parseDouble(itemCost));
				//add item to cart
				shoppingCart.add(trans);
				printWriter.println("Item: \t" + trans.getName() + "\t Price: \t$" + trans.getCost());
				//allow for multiple items to be added
				do {
					System.out.println("Is that all the items? \'[y]es\' or \'[n]o\'");
					allItems = in.nextLine();
				}while(!allItems.equals("no") && !allItems.equals("yes") && !allItems.equals("y") && !allItems.equals("n"));

			}while(allItems.equals("no") || allItems.equals("n"));
			
			//Ali's confilct START
			//pay with card feature
			System.out.print("Would you like to pay with card?(y/n):");
			askPaymentType = in.nextLine();
			//confirm Payment type
			if (askPaymentType.equals("y") || askPaymentType.equals("yes")){
				System.out.println("Customer is paying with card");
				profit += trans.getCost();
				System.out.println("Total amount of profit made: $" + profit);
			} else {
			//END CONFILCT

			//prompt user for amount tendered
			System.out.print("Please enter the cash amount tendered:");
			tendered = in.nextLine();
			
			//calculate the total of all the items  
			int total = 0;
			for(Transaction item : shoppingCart){
				total += item.getCost();
			}
		
			//calculate the change
			change = (Double.parseDouble(tendered)) - (total);
			
			profit += trans.getCost();
			
			System.out.println("Amount of change required = $" + change);
			
			//calculate the balance of the register
			balance = balance + total;
			
			  
			//Print receipt
			String askReceipt;
			do {
			  System.out.println("Would you like a receipt? \'[y]es\' or \'[n]o\'");
			  askReceipt = in.nextLine();
			}while( !askReceipt.equals("yes") && !askReceipt.equals("no") && !askReceipt.equals("y") && !askReceipt.equals("n") );

			if(askReceipt.equals("yes") || askReceipt.equals("y")){
				for(Transaction item : shoppingCart){
					System.out.println("Item: \t" + item.getName() + "\t Price: \t$" + item.getCost());
					printWriter.println("Item: \t" + item.getName() + "\t Price: \t$" + item.getCost());
				}
				System.out.println("Amount tendered: \t\t$" + tendered);
				System.out.println("Change given: \t\t\t$" + change);
				System.out.println("Total: \t\t\t\t$" + total);
				System.out.println("Profit made: $" + profit);
			}
			  }
		}
		//print cash register balance after transactions are completed
		System.out.println("Balance of the Cash Register: $" + balance);
		System.out.println("Profit Made: $" + profit);
		printWriter.close();
		//prompt user if they want to view purchase history
		String askHistory;
		do {
			  System.out.println("\nWould you like to view the purchase history \'[y]es\' or \'[n]o\'");
			  askHistory = in.nextLine();
		}while( !askHistory.equals("yes") && !askHistory.equals("no") && !askHistory.equals("y") && !askHistory.equals("n") );
		
		if(askHistory.equals("yes") || askHistory.equals("y")){
			
			try{
				bufferReader = new BufferedReader(new FileReader("saveFile.txt"));
				String line = bufferReader.readLine();
				
				while(line != null){
					System.out.println(line);
					line = bufferReader.readLine();
				}
				bufferReader.close();
			}catch(Exception e){
				System.out.println("Error message: " + e.toString());
			}	
		}
   }

}